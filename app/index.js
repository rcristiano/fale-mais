import '../node_modules/bootstrap/dist/js/bootstrap.min.js';
import angular from 'angular';
import uirouter from 'angular-ui-router';
import sanitize from 'angular-sanitize';
import routing from './config';
import home from './modules/home';

if (TEST) {
  require('angular-mocks');
}

import './sass/style.scss';

/*@ngInject*/
angular.module('app', [uirouter, sanitize, home])
.config(routing);
