import angular from 'angular';
import PriceService from './service/Price.js';
import FaresCalculator from './service/FaresCalculator.js';

export default angular.module('prices', [])
.service('PriceService', PriceService)
.service('FaresCalculator', FaresCalculator)
.name;
