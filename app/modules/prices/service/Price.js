import data from '../data';

/*@ngInject*/
class PriceService {
  constructor($q) {
    this.$q = $q;
  }

  getDdds() {
    return this.$q((resolve, reject) => {
      setTimeout(resolve(data.ddds), 1000);
    });
  }

  getPlans() {
    return this.$q((resolve, reject) => {
      setTimeout(resolve(data.plans), 1000);
    });
  }

  getPlans() {
    return this.$q((resolve, reject) => {
      setTimeout(resolve(data.plans), 1000);
    });
  }

  getCallPrices() {
    return this.$q((resolve, reject) => {
      setTimeout(resolve(data.callPrices), 1000);
    });
  }
}

export default PriceService;
