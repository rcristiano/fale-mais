import '../sass/home.scss';

/*@ngInject*/
class HomeController {
  constructor(PriceService, FaresCalculator) {
    this.PriceService = PriceService;
    this.FaresCalculator = FaresCalculator;
    this.call = {};
    this.ddds;
    this.plans;
    this.callPrices;

    this.loadData();
  }

  loadData() {
    this.PriceService.getDdds()
    .then(data => {
      this.ddds = data;
    });

    this.PriceService.getPlans()
    .then(data => {
      this.plans = data;
    });

    this.PriceService.getCallPrices()
    .then(data => {
      this.callPrices = data;
    });
  }

  change() {
  	// console.log(this.call);
  }

  showPrice(planMinutes) {
  	for (const i in this.callPrices) {
  		if (this.callPrices[i].from == this.call.from && this.callPrices[i].to == this.call.to) {
  			var baseFare = this.callPrices[i].minuteValue;
  		}
  	}

    return this.FaresCalculator.calculateFare(baseFare, this.call.duration, planMinutes);
  }
}

export default HomeController;
