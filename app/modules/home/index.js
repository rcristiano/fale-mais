import angular from 'angular';
import uirouter from 'angular-ui-router';

import prices from '../prices';

import routes from './config/routes';
import HomeController from './controller/home';

export default angular.module('home', [uirouter, prices])
.config(routes)
.controller('HomeController', HomeController)
.name;
