export default {
  ddds: ['011', '016', '017', '018'],

  plans: [{
    name: 'Falemais 30',
    minutes: 30
  }, {
    name: 'Falemais 60',
    minutes: 60
  }, {
    name: 'Falemais 120',
    minutes: 120
  }],

  callPrices: [{
    from: '011',
    to: '016',
    minuteValue: 1.90
  }, {
    from: '016',
    to: '011',
    minuteValue: 2.90
  }, {
    from: '011',
    to: '017',
    minuteValue: 1.70
  }, {
    from: '017',
    to: '011',
    minuteValue: 2.70
  }, {
    from: '011',
    to: '018',
    minuteValue: 0.90
  }, {
    from: '018',
    to: '011',
    minuteValue: 1.90
  }]
};	