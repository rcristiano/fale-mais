class FaresCalculator {
  calculateFare(baseFare, callLength, planMinutes) {
    if (!planMinutes) {
		  return baseFare * callLength;
    }

    const result = (baseFare * 1.1) * (callLength - planMinutes);

    if (result <= 0) {
    	return 0;
    }

    return parseFloat(result.toFixed(2));
  }
}

export default FaresCalculator;
