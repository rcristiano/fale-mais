import FaresCalculator from '../FaresCalculator.js';

describe('FaresCalculator', () => {
  const faresCalculator = new FaresCalculator();

  it('should calculate the base fare', () => {
    const result = faresCalculator.calculateFare(1.90, 200);
    expect(result).to.equal(380);
  });

  describe('Plans prices', () => {
    it('should be 0 if call length is under promo call length', () => {
      const result = faresCalculator.calculateFare(1.90, 20, 30);
      expect(result).to.equal(0);
    });

    it('should have an additional of 10% if call length is higher than plan length', () => {
      const result = faresCalculator.calculateFare(1.90, 60, 30);
      expect(result).to.equal(62.70);
    });
  });
});
